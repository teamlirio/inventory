<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.phxauto.controller.product.ProductProcess, java.sql.ResultSet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Products</title>
</head>
<body>
<a href="product.jsp">Add Product</a>
<% ProductProcess pp = new ProductProcess();
	ResultSet rs = pp.viewProducts();
%>

<table>

<tr>
<td width="300px">ID</td>
<td width="300px">Name</td>
<td width="300px">Description</td>
<td width="300px">Quantity</td>
<td width="300px">Brand</td>
<td width="300px">Engine</td>
<td width="300px">Manufacturer</td>
<td width="300px">Action</td>
</tr>
<% while(rs.next()) { %>
<tr>
<td><%=rs.getString("pID") %></td>
<td><%=rs.getString("pName") %></td>
<td><%= rs.getString("pDesc") %></td>
<td><%= rs.getInt("pQty") %></td>
<td><%= rs.getString("bName") %></td>
<td><%= rs.getString("eName") %></td>
<td><%= rs.getString("mName") %></td>
<td><a href="productprocess?action=edit&id=<%=rs.getString("pID") %>">Edit</a> | <a href="productprocess?action=delete&id=<%=rs.getString("pID") %>">Delete</a></td>
</tr>

<% } %>
</table>



</body>
</html>