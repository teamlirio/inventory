<%@page import="com.phxauto.controller.brand.*,java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<a href="brand.jsp">Add Brand</a>
	<br>
	<table>
		<tr>
			<td width="300px">ID</td>
			<td width="300px">Brand Name</td>
			<td width="300px">Actions</td>
		</tr>

		<%
			BrandProcess process = new BrandProcess();
			ResultSet rs = process.viewBrand();

			while (rs.next()) {
		%>
		<tr>
			<td>
				<%=
					rs.getString("bID")
				%>
			</td>
			<td>
				<%=
					rs.getString("bName")
				%>
			</td>
			<td><a href="brandprocess?id=<%=rs.getString("bID")%>&action=edit">Edit </a> |
			 <a href="brandprocess?id=<%=rs.getString("bID")%>&action=delete">Delete</a></td>
			
		</tr>
		<%} %>
	</table>
	
</body>
</html>