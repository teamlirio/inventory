<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="javax.servlet.http.HttpSession"%>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
</head>
<body>
	<%
		HttpSession session1 = request.getSession(false);

		if (session1 != null) {
			String name = (String) session1.getAttribute("UserSession");
			out.println("<h1>Welcome, " + name + "!</h1><a href='logout'>Logout</a><br>");
		
	%>
	<table>
		<tr>
			<td><a href="manageengine.jsp">Manage Engines</a></td>
			<td><a href="managemanufacturer.jsp">Manage Manufacturers</a></td>
			<td><a href="managebrand.jsp">Manage Brands</a></td>
			<td><a href="manageproduct.jsp">Manage Products</a></td>
		</tr>
	</table>
	<%} else {
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);
		out.println("Please login first.");
	} %>
</body>
</html>