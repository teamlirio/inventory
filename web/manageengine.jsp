<%@page import="com.phxauto.controller.engine.*,java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<a href="engine.jsp">Add Engine</a>
	<br>
	<table>
		<tr>
			<td width="300px">ID</td>
			<td width="300px">Engine Name</td>
			<td width="300px">Actions</td>
		</tr>

		<%
			EngineProcess process = new EngineProcess();
			ResultSet rs = process.viewEngines();

			while (rs.next()) {
		%>
		<tr>
			<td>
				<%=
					rs.getString("eID")
				%>
			</td>
			<td>
				<%=
					rs.getString("eName")
				%>
			</td>
			<td><a href="engineprocess?id=<%=rs.getString("eID")%>&action=edit">Edit </a> |
			 <a href="engineprocess?id=<%=rs.getString("eID")%>&action=delete">Delete</a></td>
			
		</tr>
		<%} %>
	</table>
	
</body>
</html>