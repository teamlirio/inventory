<%@page import="com.phxauto.controller.manufacturer.*,java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<a href="manufacturer.jsp">Add Manufacturer</a>
	<br>
	<table>
		<tr>
			<td width="300px">ID</td>
			<td width="300px">Manufacturer Name</td>
			<td width="300px">Actions</td>
		</tr>

		<%
		ManufacturerProcess process = new ManufacturerProcess();
			ResultSet rs = process.viewManufacturer();

			while (rs.next()) {
		%>
		<tr>
			<td>
				<%=
					rs.getString("mID")
				%>
			</td>
			<td>
				<%=
					rs.getString("mName")
				%>
			</td>
			<td><a href="manufacturerprocess?id=<%=rs.getString("mID")%>&action=edit">Edit </a> |
			 <a href="manufacturerprocess?id=<%=rs.getString("mID")%>&action=delete">Delete</a></td>
			
		</tr>
		<%} %>
	</table>
	
</body>
</html>