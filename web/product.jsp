<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet,com.phxauto.controller.manufacturer.*,com.phxauto.controller.engine.*,com.phxauto.controller.brand.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
<style>
select {
width:175px;
}
</style>
</head>
<body>
<%
ManufacturerProcess mp = new ManufacturerProcess();
EngineProcess ep = new EngineProcess();
BrandProcess bp = new BrandProcess();

ResultSet rsmp = mp.viewManufacturer();
ResultSet rsep = ep.viewEngines();
ResultSet rsbp = bp.viewBrand();

%>
<form action="addproduct.html" method="post">
<table>
<tr>
<td>ID</td>
<td>:</td>
<td><input type="text" name="pid"></td>
</tr>
<tr>
<td>Name</td>
<td>:</td>
<td><input type="text" name="pname"></td>
</tr>
<tr>
<td>Description</td>
<td>:</td>
<td><input type="text" name="pdesc"></td>
</tr>
<tr>
<td>Quantity</td>
<td>:</td>
<td><input type="text" name="pqty"></td>
</tr>
<tr>
<td>Brand</td>
<td>:</td>
<td><select name="pbid"><option value="---------">-----------------------------</option>
<% while(rsbp.next()) { %>
<option value="<%=rsbp.getString("bID") %>"><%= rsbp.getString("bName") %><%} %></option>
</select></td>

</tr>
<tr>
<td>Engine</td>
<td>:</td>
<td><select name="peid"><option value="---------">-----------------------------</option>
<% while(rsep.next()) { %>
<option value="<%=rsep.getString("eID") %>"><%= rsep.getString("eName") %><%} %></option>
</select></td>

</tr>
<tr>
<td>Manufacturer</td>
<td>:</td>
<td><select name="pmid"><option value="---------">-----------------------------</option>
<% while(rsmp.next()) { %>
<option value="<%=rsmp.getString("mID") %>"><%= rsmp.getString("mName") %><%} %></option>
</select></td>

</tr>
</table>
<button type="submit">Submit</button>

</form>
</body>
</html>