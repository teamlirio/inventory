package com.phxauto.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.phxauto.model.User;

@WebServlet("/login")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    private User user = new User();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		user.setUsername(request.getParameter("uname"));
		user.setPassword(request.getParameter("pword"));
		LoginProcess lp = new LoginProcess();
		ResultSet rs = lp.processUser(user);
		try {
			if(rs.next()){
				HttpSession session = request.getSession();
				session.setAttribute("UserSession", user.getUsername());
				session.setMaxInactiveInterval(10 * 60); 
				response.sendRedirect("dashboard.jsp");
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				rd.include(request, response);
				out.println("Invalid Username or Password.");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		out.close();
	}

}
