package com.phxauto.login;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.phxauto.jdbc.PhxJDBC;
import com.phxauto.model.User;
public class LoginProcess {
	private PhxJDBC phx = new PhxJDBC();
		public ResultSet processUser(User user) {
			String query = "Select * from Login where Username = ? and Password = ?";
			PreparedStatement pstate = null;
			ResultSet rs = null;
			try {
				pstate = phx.getConnection().prepareStatement(query);
				pstate.setString(1, user.getUsername());
				pstate.setString(2, user.getPassword());
				
				return pstate.executeQuery();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return rs;
		}
		
		public static void main(String[] args) {
			User user = new User();
			user.setUsername("normz1");
			user.setPassword("1234");
			ResultSet gg = new LoginProcess().processUser(user);
			
			try {
				if(gg.next()) {
					System.out.println(gg.getString("Username"));
				}
				else {
					System.out.println("wrong");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
