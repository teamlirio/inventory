package com.phxauto.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PhxJDBC {
	private final static String DRIVER = Messages.getString("PhxJDBC.DRIVER"); //$NON-NLS-1$
	private final static String URL = Messages.getString("PhxJDBC.URL"); //$NON-NLS-1$
	private final static String USER = Messages.getString("PhxJDBC.USER"); //$NON-NLS-1$
	private final static String PASS = Messages.getString("PhxJDBC.PASS"); //$NON-NLS-1$

	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL,USER,PASS);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
	
}
