package com.phxauto.model;

public class Products {
	private String pid;
	private String name;
	private String description;
	private int quantity;
	private String mid;
	private String bid;
	private String eid;
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	
	public Products() {
		
	}
	public Products(String pid, String name, String description, int quantity, String mid, String bid, String eid) {
		
		this.pid = pid;
		this.name = name;
		this.description = description;
		this.quantity = quantity;
		this.mid = mid;
		this.bid = bid;
		this.eid = eid;
	}
	
	
}
