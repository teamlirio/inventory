package com.phxauto.model;

public class Brand {
	private String id;
	private String name;
	private String originalId;
	
	public String getOriginalId() {
		return originalId;
	}
	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Brand(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Brand() {
		
	}
	
}
