package com.phxauto.controller.manufacturer;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/manufacturerprocess")
public class ManufacturerUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");
		String id = request.getParameter("id");
		ManufacturerDAO process = new ManufacturerProcess();
		if(action.equals("delete")) {
			int num = process.removeManufacturer(id);
			if(num!=0) {
				out.println("Successfully deleted.");
				out.println("<a href='managemanufacturer.jsp'>Manage Manufacturers</a>");
			}
		}
		else if(action.equals("edit")) {
			
			ResultSet results = process.viewManufacturer(id);
			try {
				while(results.next()) {
					out.println("<form action='manufacturerprocess' method='post'>");
					out.println("<input type='hidden' name='originalid' value='" + 
					results.getString("mID") + "'>");
					out.println("ID: <input type='text' name='mid' value='" + 
							results.getString("mID") + "'><br>");
					out.println("Name: <input type='text' name='mname' value='" + 
							results.getString("mName") + "'><br>");
					out.println("<button type='submit'>Update</button></form>");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String originalId = request.getParameter("originalid");
		String mID = request.getParameter("mid");
		String mName = request.getParameter("mname");
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setId(mID);
		manufacturer.setName(mName);
		manufacturer.setOriginalId(originalId);
		ManufacturerDAO process = new ManufacturerProcess();
		int num = process.updateManufacturer(manufacturer);
		if(num!=0) {
			out.println("Successfully updated.");
			out.println("<a href='managemanufacturer.jsp'>Manage Manufacturers</a>");
		}
	}


}
