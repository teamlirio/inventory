package com.phxauto.controller.manufacturer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/addmanufacturer.html")
public class ManufacturerEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setId(request.getParameter("mid"));
		manufacturer.setName(request.getParameter("mname"));
		ManufacturerDAO manufacturerProcess = new ManufacturerProcess();
		if(manufacturerProcess.addManufacturer(manufacturer)!= 0) {
			RequestDispatcher rd = request.getRequestDispatcher("manufacturer.jsp");
			rd.include(request, response);
			out.println("Successfully added!");
			out.println("<a href='managemanufacturer.jsp'>Manage Manufacturers</a>.");
		}
		out.close();
	}

}
