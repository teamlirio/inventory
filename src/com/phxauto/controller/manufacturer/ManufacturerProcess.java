package com.phxauto.controller.manufacturer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.phxauto.jdbc.PhxJDBC;
import com.phxauto.model.Manufacturer;
public class ManufacturerProcess implements ManufacturerDAO{
	private PhxJDBC phx = new PhxJDBC();

	/* (non-Javadoc)
	 * @see com.phxauto.controller.manufacturer.ManufacturerDAO#addManufacturer(com.phxauto.model.Manufacturer)
	 */
	@Override
	public int addManufacturer(Manufacturer manufacturer) {
		String query = "INSERT INTO Manufacturer values(?,?)";
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, manufacturer.getId());
			pstate.setString(2, manufacturer.getName());
			int num = pstate.executeUpdate();
			return num;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return 0;
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.phxauto.controller.manufacturer.ManufacturerDAO#viewManufacturer()
	 */
	@Override
	public ResultSet viewManufacturer() {
		String query = "Select * from Manufacturer";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	/* (non-Javadoc)
	 * @see com.phxauto.controller.manufacturer.ManufacturerDAO#viewManufacturer(java.lang.String)
	 */
	@Override
	public ResultSet viewManufacturer(String id) {
		String query = "Select * from Manufacturer where mID = ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public ResultSet removeSpecificManufacturer(String id) {
		String query = "Select * from Manufacturer where mID != ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.phxauto.controller.manufacturer.ManufacturerDAO#removeManufacturer(java.lang.String)
	 */
	@Override
	public int removeManufacturer(String id){
		String query = "DELETE from Manufacturer where mID = ?";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			return number = pstate.executeUpdate();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
	

	/* (non-Javadoc)
	 * @see com.phxauto.controller.manufacturer.ManufacturerDAO#updateManufacturer(com.phxauto.model.Manufacturer)
	 */
	@Override
	public int updateManufacturer(Manufacturer manufacturer) {
		String query = "UPDATE Manufacturer SET mID = ?, mName = ? where mID = ? ";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, manufacturer.getId());
			pstate.setString(2, manufacturer.getName());
			pstate.setString(3, manufacturer.getOriginalId());
			return number = pstate.executeUpdate();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
}
