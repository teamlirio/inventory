package com.phxauto.controller.manufacturer;

import java.sql.ResultSet;

import com.phxauto.model.Manufacturer;

public interface ManufacturerDAO {

	int addManufacturer(Manufacturer manufacturer);

	ResultSet viewManufacturer();

	ResultSet viewManufacturer(String id);

	int removeManufacturer(String id);

	int updateManufacturer(Manufacturer manufacturer);

}