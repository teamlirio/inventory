package com.phxauto.controller.brand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.phxauto.jdbc.PhxJDBC;
import com.phxauto.model.Brand;
public class BrandProcess implements BrandDAO {
	private PhxJDBC phx = new PhxJDBC();

	/* (non-Javadoc)
	 * @see com.phxauto.controller.brand.BrandDAO#addBrand(com.phxauto.model.Brand)
	 */
	@Override
	public int addBrand(Brand brand) {
		String query = "INSERT INTO Brand values(?,?)";
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, brand.getId());
			pstate.setString(2, brand.getName());
			int num = pstate.executeUpdate();
			return num;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return 0;
		
	}
	

	/* (non-Javadoc)
	 * @see com.phxauto.controller.brand.BrandDAO#viewBrand()
	 */
	@Override
	public ResultSet viewBrand() {
		String query = "Select * from Brand";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	/* (non-Javadoc)
	 * @see com.phxauto.controller.brand.BrandDAO#viewBrand(java.lang.String)
	 */
	@Override
	public ResultSet viewBrand(String id) {
		String query = "Select * from Brand where bID = ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	public ResultSet removeSpecificBrand(String id) {
		String query = "Select * from Brand where bID != ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}

	/* (non-Javadoc)
	 * @see com.phxauto.controller.brand.BrandDAO#removeBrand(java.lang.String)
	 */
	@Override
	public int removeBrand(String id){
		String query = "DELETE from Brand where bID = ?";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			return number = pstate.executeUpdate();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
	

	/* (non-Javadoc)
	 * @see com.phxauto.controller.brand.BrandDAO#updateBrand(com.phxauto.model.Brand)
	 */
	@Override
	public int updateBrand(Brand brand) {
		String query = "UPDATE Brand SET bID = ?, bName = ? where bID = ? ";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, brand.getId());
			pstate.setString(2, brand.getName());
			pstate.setString(3, brand.getOriginalId());
			return number = pstate.executeUpdate();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
}
