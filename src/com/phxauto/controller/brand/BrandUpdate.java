package com.phxauto.controller.brand;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/brandprocess")
public class BrandUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");
		String id = request.getParameter("id");
		BrandDAO process = new BrandProcess();
		if(action.equals("delete")) {
			int num = process.removeBrand(id);
			if(num!=0) {
				out.println("Successfully deleted.");
				out.println("<a href='managebrand.jsp'>Manage Brands</a>");
			}
		}
		else if(action.equals("edit")) {
			
			ResultSet results = process.viewBrand(id);
			try {
				while(results.next()) {
					out.println("<form action='brandprocess' method='post'>");
					out.println("<input type='hidden' name='originalid' value='" + 
					results.getString("bID") + "'>");
					out.println("ID: <input type='text' name='bid' value='" + 
							results.getString("bID") + "'><br>");
					out.println("Name: <input type='text' name='bname' value='" + 
							results.getString("bName") + "'><br>");
					out.println("<button type='submit'>Update</button></form>");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String originalId = request.getParameter("originalid");
		String bID = request.getParameter("bid");
		String bName = request.getParameter("bname");
		Brand brand = new Brand();
		brand.setId(bID);
		brand.setName(bName);
		brand.setOriginalId(originalId);
		BrandDAO process = new BrandProcess();
		int num = process.updateBrand(brand);
		if(num!=0) {
			out.println("Successfully updated.");
			out.println("<a href='managebrand.jsp'>Manage Brands</a>");
		}
	}


}
