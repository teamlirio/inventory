package com.phxauto.controller.brand;

import java.sql.ResultSet;

import com.phxauto.model.Brand;

public interface BrandDAO {

	int addBrand(Brand brand);

	ResultSet viewBrand();

	ResultSet viewBrand(String id);

	int removeBrand(String id);

	int updateBrand(Brand brand);

}