package com.phxauto.controller.brand;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/addbrand.html")
public class BrandEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Brand brand = new Brand();
		brand.setId(request.getParameter("bid"));
		brand.setName(request.getParameter("bname"));
		BrandDAO brandProcess = new BrandProcess();
		if(brandProcess.addBrand(brand)!= 0) {
			RequestDispatcher rd = request.getRequestDispatcher("brand.jsp");
			rd.include(request, response);
			out.println("Successfully added!");
			out.println("<a href='managebrand.jsp'>Manage Brands</a>.");
		}
		out.close();
	}

}
