package com.phxauto.controller.engine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.phxauto.jdbc.PhxJDBC;
import com.phxauto.model.Engine;
public class EngineProcess implements EngineDAO {
	private PhxJDBC phx = new PhxJDBC();

	public int addEngine(Engine engine) {
		String query = "INSERT INTO Engine values(?,?)";
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, engine.getId());
			pstate.setString(2, engine.getName());
			int num = pstate.executeUpdate();
			return num;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return 0;
		
	}
	

	public ResultSet viewEngines() {
		String query = "Select * from Engine";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
	
			pstate = phx.getConnection().prepareStatement(query);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	public ResultSet viewEngines(String id) {
		String query = "Select * from Engine where eID = ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public ResultSet removeSpecificEngine(String id) {
		String query = "Select * from Engine where eID != ?";
		ResultSet result = null;
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			result = pstate.executeQuery();
			return result;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	

	public int removeEngine(String id){
		String query = "DELETE from Engine where eID = ?";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			return number = pstate.executeUpdate();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
	

	public int updateEngine(Engine engine) {
		String query = "UPDATE Engine SET eID = ?, eName = ? where eID = ? ";
		PreparedStatement pstate = null;
		int number = 0;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, engine.getId());
			pstate.setString(2, engine.getName());
			pstate.setString(3, engine.getOriginalId());
			number = pstate.executeUpdate();
			return number;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return number;
		
	}
	
	public static void main(String[] args) {
		PhxJDBC p = new PhxJDBC();
		System.out.println(p.getConnection());
		String query = "Select * from Engine";
		try {
			PreparedStatement pstate = p.getConnection().prepareStatement(query);
			ResultSet r = pstate.executeQuery();
			
			while(r.next()) {
				System.out.println(r.getString("eID"));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
