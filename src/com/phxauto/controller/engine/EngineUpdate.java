package com.phxauto.controller.engine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/engineprocess")
public class EngineUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String action = request.getParameter("action");
		String id = request.getParameter("id");
		EngineProcess process = new EngineProcess();
		if(action.equals("delete")) {
			int num = process.removeEngine(id);
			if(num!=0) {
				out.println("Successfully deleted.");
				out.println("<a href='manageengine.jsp'>Manage Engines</a>");
			}
		}
		else if(action.equals("edit")) {
			
			ResultSet results = process.viewEngines(id);
			try {
				while(results.next()) {
					out.println("<form action='engineprocess' method='post'>");
					out.println("<input type='hidden' name='originalid' value='" + 
					results.getString("eID") + "'>");
					out.println("ID: <input type='text' name='eid' value='" + 
							results.getString("eID") + "'><br>");
					out.println("Name: <input type='text' name='ename' value='" + 
							results.getString("eName") + "'><br>");
					out.println("<button type='submit'>Update</button></form>");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String originalId = request.getParameter("originalid");
		String eID = request.getParameter("eid");
		String eName = request.getParameter("ename");
		Engine engine = new Engine();
		engine.setId(eID);
		engine.setName(eName);
		engine.setOriginalId(originalId);
		EngineProcess process = new EngineProcess();
		int num = process.updateEngine(engine);
		if(num!=0) {
			out.println("Successfully updated.");
			out.println("<a href='manageengine.jsp'>Manage Engines</a>");
		}
	}


}
