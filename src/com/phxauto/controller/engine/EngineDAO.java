package com.phxauto.controller.engine;

import java.sql.ResultSet;

import com.phxauto.model.Engine;

public interface EngineDAO {

	int addEngine(Engine engine);

	ResultSet viewEngines();

	int removeEngine(String id);

	int updateEngine(Engine engine);

}