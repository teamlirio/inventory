package com.phxauto.controller.engine;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.phxauto.model.*;

@WebServlet("/addengine.html")
public class EngineEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Engine engine = new Engine();
		engine.setId(request.getParameter("eid"));
		engine.setName(request.getParameter("ename"));
		EngineDAO engineProcess = new EngineProcess();
		if(engineProcess.addEngine(engine)!= 0) {
			RequestDispatcher rd = request.getRequestDispatcher("engine.jsp");
			rd.include(request, response);
			out.println("Successfully added!");
			out.println("<a href='manageengine.jsp'>Manage Engines</a>.");
		}
		out.close();
	}

}
