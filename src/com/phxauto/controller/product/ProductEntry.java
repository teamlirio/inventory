package com.phxauto.controller.product;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.phxauto.model.*;

@WebServlet("/addproduct.html")
public class ProductEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Products product = new Products();
		product.setPid(request.getParameter("pid"));
		product.setName(request.getParameter("pname"));
		product.setDescription(request.getParameter("pdesc"));
		product.setQuantity(Integer.parseInt(request.getParameter("pqty")));
		product.setEid(request.getParameter("peid"));
		product.setMid(request.getParameter("pmid"));
		product.setBid(request.getParameter("pbid"));
		
		ProductProcess pp = new ProductProcess();
		int rows = pp.addProduct(product);
		if(rows > 0) {
			RequestDispatcher rd = request.getRequestDispatcher("product.jsp");
			rd.include(request, response);
			out.println("Successfully added!");
			out.println("<a href='manageproduct.jsp'>Manage Products</a>");
		}
		
	}

}
