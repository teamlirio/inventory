package com.phxauto.controller.product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.phxauto.jdbc.PhxJDBC;
import com.phxauto.model.Products;
import com.phxauto.model.*;
public class ProductProcess {
	PhxJDBC phx = new PhxJDBC();
	public int addProduct(Products p) {
		String query = "INSERT INTO Product values(?,?,?,?,?,?,?)";
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, p.getPid());
			pstate.setString(2, p.getName());
			pstate.setString(3, p.getDescription());
			pstate.setInt(4, p.getQuantity());
			pstate.setString(5, p.getBid());
			pstate.setString(6, p.getMid());
			pstate.setString(7, p.getEid());
			
			return pstate.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public ResultSet viewProducts() {
		String query = "Select * from Product p LEFT JOIN Manufacturer m ON p.mID = m.mID LEFT JOIN Engine e ON p.eID = e.eID LEFT JOIN Brand b ON p.bID = b.bID";
		PreparedStatement pstate = null;
		ResultSet rs = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			rs = pstate.executeQuery();
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}
	public ResultSet editProduct(String id) {
		String query = "Select * from Product p LEFT JOIN Manufacturer m ON p.mID = m.mID LEFT JOIN Engine e ON p.eID = e.eID LEFT JOIN Brand b ON p.bID = b.bID where pID = ?";
		PreparedStatement pstate = null;
		ResultSet rs = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			rs = pstate.executeQuery();
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}
	
	public int removeProduct(String id) {
		String query = "DELETE FROM Product where pID = ?";
		
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, id);
			return pstate.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		}
		return 0;
	}
	
	public int updateProduct(Products product, String id) {
		String query = "UPDATE Product SET pID = ?, pName = ?, pDesc = ?, pQty = ?, bID = ?, mID = ?, eID = ? where pID = ?";
		PreparedStatement pstate = null;
		try {
			pstate = phx.getConnection().prepareStatement(query);
			pstate.setString(1, product.getPid());
			pstate.setString(2, product.getName());
			pstate.setString(3, product.getDescription());
			pstate.setInt(4, product.getQuantity());
			pstate.setString(5, product.getBid());
			pstate.setString(6, product.getMid());
			pstate.setString(7, product.getEid());
			pstate.setString(8, id);
			
			return pstate.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
